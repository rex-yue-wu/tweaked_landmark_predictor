# -*- coding: utf-8 -*-
"""
This script uses the Tweaked Facial Landmark model(s) 
and predicts facial landmarks on ALFW and AFW data.
Results and visualization images are saved under ../expts/test_on_alfw_afw

Created on Tue May 23 01:20:48 2017
@author: yue_wu
"""

import os
import sys
file_path = os.path.realpath( __file__ )
repo_root = os.path.join( os.path.dirname( file_path ), os.path.pardir )
model_root = os.path.join( repo_root, 'models' )
data_root = os.path.join( repo_root, 'sample_data' )
expt_root = os.path.join( repo_root, 'expts' )
sys.path.insert( 0, os.path.join( repo_root, 'libs', 'tweaked' ) )
from core import TweakFacialLandmark, Normalize
from utils import load_stats, image_montage, mse_io_np
from sklearn.externals.joblib import load
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
import cv2

#################################################################################
# Load Tweaked Facial Landmark Detector
#################################################################################
joinpath = lambda f : os.path.join( model_root, f )

engine = TweakFacialLandmark( \
	    baseline_weight_file = joinpath( 'eccv14_baseline.h5' ),\
	    cluster_weight_file = joinpath( 'pca-gmm-cluster_C64-at_N-4.npy' ),\
	    finetune_weight_file_list = [ joinpath( 'finetune_cluster_%d.h5' % idx ) for idx in range( 64 ) ] )

#################################################################################
# Load Training Data Statistics
#################################################################################
X_mean, X_std = load_stats( ( joinpath( 'ECCV14-h40-w40-d3-v0.10-mse_io-adam-X-mean.npy' ), joinpath( 'ECCV14-h40-w40-d3-v0.10-mse_io-adam-X-std.npy' ) ) )
Y_mean, Y_std = load_stats( ( joinpath( 'ECCV14-h40-w40-d3-v0.10-mse_io-adam-Y-mean.npy' ), joinpath( 'ECCV14-h40-w40-d3-v0.10-mse_io-adam-Y-std.npy' ) ) )
X_rule = lambda D, D_mean, D_std : (D-D_mean)/(1e-6+D_std)
Y_rule = lambda D, D_mean, D_std : (D-0.5) * 2
prep_X_norm = Normalize( X_mean, X_std, X_rule )
prep_Y_norm = Normalize( Y_mean, Y_std, Y_rule )

def prepare_normalized_data( X, Y = None ) :
    '''
    Prepare raw input data
    X = tensor 4D, shape of ( n_samples, 3, 40, 40 ), dtype = uint8, range in ( 0, 255 )
    Y = tensor 2D, shape of ( n_samples, 10 ) or ( n_samples, 5, 2 ), dtype = float32, range in ( 0, 40 )
    '''
    if ( X.dtype == 'uint8' ) :
	X = X.astype( np.float32 )
    X /= 255.
    if ( Y is None ) :
	return prep_X_norm.transform( X )
    else :
	Y /= 40.
	return prep_X_norm.transform( X ), prep_Y_norm.transform( Y )

def visualize( bX, bY, bZ, debug_ncols = 8, output_file = 'debug.jpg', display_err = False ) :
    sample_images = []
    debug_nrows = bX.shape[0] / debug_ncols
    err_list = []
    for x, y, z in zip( bX, bY, bZ ) :
	face_image = np.rollaxis( np.round( ( ( x * X_std ) + X_mean ) * 255 ).astype( 'uint8' ), 0, 3 )
	debug_image = cv2.resize( face_image, (200, 200) )
	lm_true_xy = y.reshape([-1,2]) /2 + .5
	lm_pred_xy = z.reshape([-1,2]) /2 + .5
	for xx, yy in lm_true_xy :
	    cv2.circle( debug_image, ( int(xx * 200), int(yy * 200) ), 4, color = (255,255,0), thickness = -1 )
	for xx, yy in lm_pred_xy :
	    cv2.circle( debug_image, ( int(xx * 200), int(yy * 200) ), 4, color = (255,0,255), thickness = -1 )
	err = mse_io_np( lm_true_xy, lm_pred_xy )
	err_list.append( err )
	if ( display_err ) :
	    cv2.putText( debug_image, "%.2f" % err, ( 10, 200 ), fontScale = 1, fontFace = 0, color = (255,255,255), thickness = 3 )
	sample_images.append( debug_image )
   # save these sample images
    montage_image = image_montage( sample_images, 200, debug_nrows, 200, debug_ncols, 5, debug_image.ndim )
    cv2.imwrite( output_file, montage_image );
    print "INFO: Successfully save debug image to", output_file
    if ( display_err ) :
	print "INFO: err =", np.array( [ np.percentile( err_list, p ) for p in range(1,100,5) ] )
    return
#################################################################################
# Load Testing Data
#################################################################################
testing_XY  = [ ( os.path.join( data_root, 'set-testing-alfw_2995_data-X.npy' ),
		  os.path.join( data_root, 'set-testing-alfw_2995_data-Y.npy' ) ),
		( os.path.join( data_root, 'set-testing-afw_337_data-X.npy' ),
		  os.path.join( data_root, 'set-testing-afw_337_data-Y.npy') ) ]

output_dir = os.path.join( expt_root, 'test_on_alfw_afw' )
for this_X_file, this_Y_file in testing_XY :
    tX, tY = prepare_normalized_data( load( this_X_file ), load( this_Y_file ) )
    dataset_name = this_X_file.split('-')[-2]
    # landmark prediction
    y_baseline = engine.predict_normalized_sample( tX, 'baseline' )
    y_tweak_hard = engine.predict_normalized_sample( tX, 'tweak_hard' )
    y_tweak_soft = engine.predict_normalized_sample( tX, 'tweak_soft' )
    # error computation
    err0 = [ mse_io_np( y_true.reshape([-1,2]), y_pred.reshape([-1,2]) ) for y_true, y_pred in zip( tY, y_baseline ) ]
    err1 = [ mse_io_np( y_true.reshape([-1,2]), y_pred.reshape([-1,2]) ) for y_true, y_pred in zip( tY, y_tweak_hard ) ]
    err2 = [ mse_io_np( y_true.reshape([-1,2]), y_pred.reshape([-1,2]) ) for y_true, y_pred in zip( tY, y_tweak_soft ) ]
    # compute graph
    perc_list = [1e-3,1e-2,1e-1] + range( 1, 100 ) + [ 100-1e-1, 100-1e-2, 100-1e-3 ]
    per0 = [ np.percentile( err0, p ) for p in perc_list ]
    per1 = [ np.percentile( err1, p ) for p in perc_list ]
    per2 = [ np.percentile( err2, p ) for p in perc_list ]
    per = np.column_stack( [ per0, per1, per2 ] )
    # draw graph
    pyplot.figure( figsize=(6,6) )
    pyplot.plot( per, np.column_stack( [ perc_list, perc_list, perc_list ] ) )
    pyplot.legend( [ 'baseline', 'tweak-hard', 'tweak-soft' ], loc=4 )
    pyplot.xlabel( 'normalized MSE w.r.t. inter-occular distance')
    pyplot.ylabel( 'percentage of data' )
    perf_file = os.path.join( output_dir, 'perf-%s.png' % dataset_name )
    pyplot.savefig( perf_file, dpi=200, bbox_inches='tight' )
    # visualize prediction results
    nb_samples = tY.shape[0]
    ridx = np.random.choice( range( nb_samples ), 256 ).tolist()
    visualize( tX[ridx], tY[ridx], y_baseline[ridx].reshape( [ -1, 10 ] ), output_file = os.path.join( output_dir, 'vis-%s-baseline.jpg' % dataset_name ), display_err = True )
    visualize( tX[ridx], tY[ridx], y_tweak_hard[ridx].reshape( [ -1, 10 ] ), output_file = os.path.join( output_dir, 'vis-%s-tweak_hard.jpg' % dataset_name ), display_err = True )
    visualize( tX[ridx], tY[ridx], y_tweak_soft[ridx].reshape( [ -1, 10 ] ), output_file = os.path.join( output_dir, 'vis-%s-tweak_soft.jpg' % dataset_name ), display_err = True )
    # verbose print
    print "-" * 100
    print "INFO: dataset =", dataset_name
    print "INFO: baseline   mean( mse_io ) =", np.mean( err0 ), "median( mse_io ) =", np.median( err0 )
    print "INFO: tweak-hard mean( mse_io ) =", np.mean( err1 ), "median( mse_io ) =", np.median( err1 )
    print "INFO: tweak-soft mean( mse_io ) =", np.mean( err2 ), "median( mse_io ) =", np.median( err2 )


# You are expected to see the following print out messages
'''
----------------------------------------------------------------------------------------------------
INFO: dataset = alfw_2995_data
INFO: baseline   mean( mse_io ) = 6.00854787102 median( mse_io ) = 5.44322456937
INFO: tweak-hard mean( mse_io ) = 5.56222411521 median( mse_io ) = 5.14399245097
INFO: tweak-soft mean( mse_io ) = 5.54448001181 median( mse_io ) = 5.12496645359
----------------------------------------------------------------------------------------------------
INFO: dataset = afw_337_data
INFO: baseline   mean( mse_io ) = 5.15679293834 median( mse_io ) = 4.62863069454
INFO: tweak-hard mean( mse_io ) = 4.67693282625 median( mse_io ) = 4.31811148373
INFO: tweak-soft mean( mse_io ) = 4.65699015041 median( mse_io ) = 4.31557707787
'''
