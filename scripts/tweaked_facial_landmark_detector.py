#!/bin/env python
"""
This is the standalone Tweaked Facial Landmark Predictor

Dependency:
    numpy.version = 1.12.1
    keras.version = 1.2.1
    keras.backend = 'theano'
    theano.version = 0.9

Created on Tue May 23 10:51:25 2017

@author: yue_wu
"""
import os
import sys
file_path = os.path.realpath( __file__ )
repo_root = os.path.join( os.path.dirname( file_path ), os.path.pardir )
model_root = os.path.join( repo_root, 'models' )
data_root = os.path.join( repo_root, 'sample_data' )
expt_root = os.path.join( repo_root, 'expts' )
sys.path.insert( 0, os.path.join( repo_root, 'libs', 'tweaked' ) )
from core import TweakFacialLandmark, Normalize
from utils import load_stats, load_image_file, detect_one_face
import numpy as np
import cv2

#################################################################################
# Load Tweaked Facial Landmark Detector
#################################################################################
joinpath = lambda f : os.path.join( model_root, f ) 

engine = TweakFacialLandmark( \
            baseline_weight_file = joinpath( 'eccv14_baseline.h5' ),\
            cluster_weight_file = joinpath( 'pca-gmm-cluster_C64-at_N-4.npy' ),\
            finetune_weight_file_list = [ joinpath( 'finetune_cluster_%d.h5' % idx ) for idx in range( 64 ) ] )

#################################################################################
# Load Training Data Statistics
#################################################################################
X_mean, X_std = load_stats( ( joinpath( 'ECCV14-h40-w40-d3-v0.10-mse_io-adam-X-mean.npy' ), joinpath( 'ECCV14-h40-w40-d3-v0.10-mse_io-adam-X-std.npy' ) ) )
Y_mean, Y_std = load_stats( ( joinpath( 'ECCV14-h40-w40-d3-v0.10-mse_io-adam-Y-mean.npy' ), joinpath( 'ECCV14-h40-w40-d3-v0.10-mse_io-adam-Y-std.npy' ) ) )
X_rule = lambda D, D_mean, D_std : (D-D_mean)/(1e-6+D_std)
Y_rule = lambda D, D_mean, D_std : (D-0.5) * 2
prep_X_norm = Normalize( X_mean, X_std, X_rule )
prep_Y_norm = Normalize( Y_mean, Y_std, Y_rule )

def prepare_normalized_data( X, Y = None ) :                                                                                                          
    '''                                                                                                                                               
    Prepare raw input data                                                                                                                            
    X = tensor 4D, shape of ( n_samples, 3, 40, 40 ), dtype = uint8, range in ( 0, 255 )                                                              
    Y = tensor 2D, shape of ( n_samples, 10 ) or ( n_samples, 5, 2 ), dtype = float32, range in ( 0, 40 )                                             
    '''
    if ( X.dtype == 'uint8' ) :
        X = X.astype( np.float32 )
    X /= 255.
    if ( Y is None ) :
        return prep_X_norm.transform( X )
    else :
        Y /= 40.
        return prep_X_norm.transform( X ), prep_Y_norm.transform( Y )
        
def prepare_face_landmark_inputs( image_file_list ) :
    '''This is a util function to 1) detect face, 2) normalize one face for landmark prediction
    '''
    output_lut = dict()
    for this_file in image_file_list :
        this_image = load_image_file( this_file )
        this_image_bbox_list = detect_one_face( this_image )
        if ( not this_image_bbox_list ) :
            print "INFO: early stop facial landmark detection for image %s due to no positive face detections" % this_file
            continue
        this_face_tensor = []
        for top, bot, left, right in this_image_bbox_list :
            cropped_face = this_image[ top:bot+1, left:right+1 ]
            ch, cw = cropped_face.shape[:2]
            resized_face = np.expand_dims( np.rollaxis( cv2.resize( cropped_face, ( 40, 40 ) ), 2, 0 ), axis = 0 )
            this_face_tensor.append( resized_face )
        this_face_tensor = np.concatenate( this_face_tensor, axis = 0 ) 
        output_lut[ this_file ] = [ this_face_tensor, this_image_bbox_list ]
    return output_lut

def wrapper_landmark_detector( sample_image_file_list, output_csv = 'facial_landmark.csv', mode = 'baseline', visualization_dir = None ) :
    '''This is the wrapper function for facial landmark detection using the tweaked facial landmark detectors
    INPUT:
        sample_image_file_list = list of image files
        ouptut_csv = file path to output landmark detection csv 
        mode = string in { 'baseline', 'tweak_hard', 'tweak_soft' }, predictor mode
               baseline: use baseline model, i.e. w/o tweaked settings
               tweak_hard: use baseline's CNN for featex, and the predicted clusterwise tweaked model for prediction
               tweak_soft: use baseline's CNN for featex, all clusterwise tweaked models for prediction, and soft membership for aggregation
        visualize = bool, whether or not save visualization results
    '''
    def visualize_landmark_on_face( image, bbox, landmark ) :
        top, bot, left, right = bbox 
        thickness = max( 2, int( min( right - left, bot - top ) * .025 ) )
        cv2.rectangle( image, ( left, top ), ( right, bot ), color = ( 255, 255, 0 ), thickness = thickness )
        for x, y in landmark :
            cv2.circle( image, ( int(x), int(y) ), thickness, color = (0,255,255), thickness = -1 )
        return image        
    input_face_lut = prepare_face_landmark_inputs( sample_image_file_list )
    output_lines = [ ",".join( [ 'image_file_path', 'face_top', 'face_bot', 'face_left', 'face_right', \
                                 "left_eye_x", "left_eye_y", "right_eye_x", "right_eye_y", "nose_x", \
                                 "nose_y", "left_mouth_x", "left_mouth_y", "right_mouth_x", "right_mouth_y" ] ) ]
    if ( visualization_dir is None ) :
        visualize = False
    else :
        visualize = True
    for key, X_and_bbox in input_face_lut.iteritems() :
        X, bbox_list = X_and_bbox
        Xn = prepare_normalized_data( X )
        lms = engine.predict_normalized_sample( Xn, mode = mode )
        this_output = []
        for bbox, lm in zip( bbox_list, lms ) :
            top, bot, left, right = bbox
            bH, bW = ( bot - top + 1 ), ( right - left + 1 )
            # restore landmark to the original coordinate system
            restored_lm = np.column_stack( [ shift + ( lm_d / 2 + 0.5 ) * d for lm_d, d, shift in zip( lm.T, [ bW, bH ], [ left, top ] ) ] )
            this_line = [ key, bbox, restored_lm ]
            this_output.append( this_line )
        if ( visualize ) :
            this_debug_file = os.path.join( visualization_dir, 'vis-' + mode + '-' + os.path.basename( key ) )
            debug = load_image_file( key )
            for key, bbox, landmark in this_output :
                debug = visualize_landmark_on_face( debug, bbox, landmark )
            cv2.imwrite( this_debug_file, debug )
            print "INFO: successfully visualization results to debug file", this_debug_file
        output_lines += [ ",".join( [ key ] + [ "%d" % int(v) for v in bbox ] + [ "%.2f" % v for v in landmark.ravel() ] ) for key, bbox, landmark in this_output ]
    # write all results to file
    with open( output_csv, 'w' ) as OUT :
        OUT.write( "\n".join( output_lines ) + "\n" )
    print "INFO: successfully save all detection results to", output_csv
    return

import sys
import argparse
if __name__ == '__main__' :
    """Keras Tweaked CNN for facial landmark detector
    Usage:
        tweaked_facial_landmark_detector.py -h
    """
    parser = argparse.ArgumentParser( description = 'Keras Tweaked CNN for facial landmark detector' )
    parser.add_argument( '-f',  action = "append", dest = "input_files", default = [], help = "input test image file path" )
    parser.add_argument( '-l',  action = "store", dest = "input_flist", default = None, help = "input test image file list, each line is a file" )
    parser.add_argument( '-o',  action = "store", dest = "output_det_file", default = "det_landmark.csv", help = "output detected landmark csv" )
    parser.add_argument( '-m', choices = ( "baseline", "tweak_soft", "tweak_hard", "demo" ), dest = "mode", default = "tweak_soft", help = "detector working modes" )
    parser.add_argument( '-d',  action = "store", dest = "visualization_dir", default = None,help = "output visualization dir for debugging" )
    parser.add_argument( '--version', action='version', version='%(prog)s 1.0' )
    # parse program arguments
    res = parser.parse_args()
    input_files = res.input_files
    input_flist = res.input_flist
    output_csv  = res.output_det_file
    mode = res.mode
    debug_dir = res.visualization_dir
    # unify detector inputs
    if ( input_flist is not None ) :
        assert os.path.isfile( input_flist ), "ERROR: cannot locate input image file list %s" % input_flist
        with open( input_flist, 'r' ) as IN :
            image_files = [ line.strip() for line in IN.readlines() ]
    elif ( len( input_files ) > 0 ):
        image_files = input_files
    elif ( mode == 'demo' ):
        input_files = [ os.path.join( data_root, f ) for f in [ 'Zeng_Qinghong_0001.jpg', 'Mickey_Gilley_0001.jpg', 'obama-peru-1120.jpg', 'Trump-kids.jpg' ] ]
    else :
        print "ERROR: no input file or input file list is given, exit"
        exit(-1)
    # verify image file path
    valid_files = []
    for this_file in image_files : 
        if ( os.path.isfile( this_file ) ) :
            valid_files.append( this_file )
        else :
            print "WARNING: cannot locate input image file", this_file
    # overwrite settings if mode == 'demo'
    if ( mode == 'demo' ) :
        for mode in [ 'baseline', 'tweak_soft', 'tweak_hard' ] :
            output_csv = os.path.join( expt_root, 'demo', 'detected_landmark_%s.csv' % mode )
            wrapper_landmark_detector( valid_files, output_csv = output_csv, mode = mode, visualization_dir = os.path.join( expt_root, 'demo' ) )
    else :
        wrapper_landmark_detector( valid_files, output_csv = output_csv, mode = mode, visualization_dir = debug_dir )
