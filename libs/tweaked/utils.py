import os
import sys
import numpy as np
from sklearn.externals.joblib import load, dump
import dlib
import cv2

def load_stats( stat_tuple ) :
    '''Load input image stats
    '''
    mean_stat_file, std_stat_file = stat_tuple
    return ( np.load( mean_stat_file ), np.load( std_stat_file ) )

def image_montage( weights, filt_rows, numof_rows, filt_cols, numof_cols, b, numof_chs, border_color = 'w' ) :
   # create an output image
    image_buffer = [];
    if ( numof_chs == 3 ) :
	col_border = np.zeros( [ filt_rows, b, 3 ], dtype='uint8');
	row_border = np.zeros( [ b, (filt_cols+b)*numof_cols-b, 3 ], dtype='uint8');
    else :
	col_border = np.zeros( [ filt_rows, b ], dtype='uint8');
	row_border = np.zeros( [ b, (filt_cols+b)*numof_cols-b ], dtype='uint8');
    if ( border_color == 'w' ) :
	col_border += 255;
	row_border += 255;
    for r in range( numof_rows ) :
	row_buffer = [];
	for c in range( numof_cols ) :
	    k = r * numof_cols + c;
	    row_buffer.append( weights[k] );
	    row_buffer.append( col_border );
	row_buffer.pop(-1);
	# merge all cols together
	image_buffer.append( np.column_stack( row_buffer ) );
	image_buffer.append( row_border );
    image_buffer.pop(-1);
    image_buffer = np.row_stack( image_buffer );
    return image_buffer

def mse_io_np( y_true, y_pred, eye_indices = [0,1]) :
    left, right = eye_indices;
    dist_io = np.sqrt( ( (y_true[left,:] - y_true[ right,:] ) ** 2 ).sum(axis=-1) )
    return np.mean( np.sqrt( ( (y_pred - y_true) ** 2).mean(axis=-1) ) / ( 1e-10 + dist_io) )


#################################################################################
# Image and Face Detection
#################################################################################
def load_image_file( image_file_path ) :
    '''Read image file from disk
    INPUT:
	image_file_path = string, path to image file
    OUTPUT:
	image_array = np.ndarray, shape of ( height, width, 3 )
    '''
    if ( os.path.isfile( image_file_path ) ) :
	image = cv2.imread( image_file_path, 1 )
	if ( image.ndim == 2 ) :
	    image = np.dstack( [ image, image, image ]  )
	return image
    else :
	return None

# initialize DLIB face detector
detector = dlib.get_frontal_face_detector();
def detect_one_face( full_image, initial_bbox = None, relax = 3, facebbox_fmt = 'tblr' ) :
    '''
    Detect a face image using default face detector
    INPUT:
	full_image  = np.ndarray, shape of ( height, width, 3 )
	initial_bbox = None or [ x, y, w, h ], initial face bbox
	relax = float, relax x$relax factor of the initial bbox
	facebbox_fmt = string in { 'tblr', 'xywh' }, output bbox format
    OUTPUT:
	bbox_list = list of bboxes, each element is a face bbox of format ${facebbox_fmt}
    '''
    height, width = full_image.shape[:2];
    # 1. relax initial bbox to be bigger
    if ( initial_bbox is None ) :
	x, y, w, h = 0, 0, width, height
    else :
	x, y, w, h = initial_bbox;
    hw = w * .5;
    hh = h * .5;
    cx, cy = x+hw, y+hh;
    nw = hw * ( 1 + relax );
    nh = hh * ( 1 + relax );
    left  = int( max( 0, cx - nw ) );
    top   = int( max( 0, cy - nh ) );
    right = int( min( cx + nw, width ) );
    bot   = int( min( cy + nh, height ) );
    # 2. detect image within this tight bbox
    dets = detector( np.array( full_image[ top:bot, left:right ], dtype = 'uint8' ) );
    if ( len( dets ) == 0 ) :
	return []
    else :
	bbox_list = []
	for det in dets :
	    d_top, d_bot, d_left, d_right = [ det.top(), det.bottom(), det.left(), det.right() ]
	    # 3. offset w.r.t. original coordinates
	    if   ( facebbox_fmt == 'tblr' ) :
		# format : top, bot, left, right
		bbox_list.append( [ d_top + top, d_bot + top, d_left + left, d_right + left ] )
	    elif ( facebbox_fmt == 'xywh' ) :
		# format : face_x, face_y, face_width, face_height
		bbox_list.append( [ d_left + left, d_top + top, d_right - d_left, d_bot - d_top ] )
	    else :
		raise NotImplementedError, "ERROR: facebbox_fmt %s is NOT supported" % facebbox_fmt
	return bbox_list
