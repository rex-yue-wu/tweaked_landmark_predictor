import os
import cv2
import sys
import numpy as np
file_path = os.path.realpath( __file__ )
lib_root  = os.path.dirname( file_path )
keras_lib = os.path.join( lib_root, os.path.pardir, 'keras_1.2.0/' )
assert os.path.isdir( keras_lib )
sys.path.insert( 0, keras_lib )
import keras
assert keras.__version__ == '1.2.0', "ERROR: only keras v1.2.0 is tested, but current version =%s" %  keras.__version__
from keras.models import Sequential, Model
from keras.layers.core import Dense
from keras.layers.core import Dense, Reshape
from keras import backend as K
from sklearn.externals.joblib import load, dump
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D


class TweakFacialLandmark( object ) :
    def __init__( self, baseline_weight_file,
                  cluster_weight_file,
                  finetune_weight_file_list ) :
        assert os.path.isfile( baseline_weight_file ), "ERROR: cannot locate baseline model weights %s" % baseline_weight_file
        self.baseline = self._create_baseline_model( baseline_weight_file )
        print "#" * 100
        print "INFO: successfully load baseline model with architecture"
        print self.baseline.summary()
        self.featex = self._create_featex_model()
        print "#" * 100
        print "INFO: successfully load featex model with architecture"
        print self.featex.summary()
        self.finetune_list = []
        print "#" * 100
        print "INFO: successfully load finetuned tweaked model with architecture"
        for idx, this_finetune_weight_file in enumerate( finetune_weight_file_list ) :
            this_finetune = self._create_clusterwise_model( this_finetune_weight_file )
            self.finetune_list.append( this_finetune )
        print this_finetune.summary()
        print "#" * 100
        print "INFO: successfully load feature clustering model"
        self.cluster = self._load_cluster_model( cluster_weight_file )
        print self.cluster
        assert len( self.finetune_list ) == self.cluster.steps[-1][1].n_components, \
            "ERROR: the number of clusterwise finetuned model %d != the number of clusters %d" % ( len( self.finetune_list ), self.cluster.steps[-1][1].n_component )
        return
    def _load_cluster_model( self, cluster_weight_file ) :
        '''Create sklearn clustering model
        '''
        return load( cluster_weight_file )
    def _create_baseline_model( self, model_weight_file, target_shape = ( 3, 40, 40 ), nb_hidden_nodes = 128 ) :
        '''Create baseline facial landmark detection model
        '''
        def abstanh( x ) :
            return K.abs( K.tanh( x ) )
        model_name = os.path.basename( model_weight_file ).rsplit('.')[0]
        model = Sequential()
        # conv1
        model.add( Convolution2D( 16, 5, 5, border_mode='same', activation= abstanh, input_shape = target_shape, name = model_name + '-conv1' ))
        model.add( MaxPooling2D((2,2), name = model_name + '-maxpool1' ) )
        # model.add( Dropout( 0.25 ) )
        # conv2
        model.add(Convolution2D( 48, 3, 3, border_mode='same', activation= abstanh, name = model_name + '-conv2' ) )
        model.add(MaxPooling2D( (2, 2), name = model_name + '-maxpool2'))
        # model.add( Dropout( 0.25 ) )
        # conv3
        model.add(Convolution2D( 64, 3, 3, activation= abstanh, name = model_name + '-conv3' ) )
        model.add(MaxPooling2D( (2, 2), name = model_name + '-maxpool3'))
        # model.add( Dropout( 0.25 ) )
        # conv4
        model.add(Convolution2D( 64, 2, 2, activation= abstanh, name = model_name + '-conv4'  ) )
        model.add( Flatten( name = 'flatten'))
        model.add( Dense( nb_hidden_nodes, name = model_name + '-dense1' ) )
        # activation
        model.add( Activation( 'tanh', name = model_name + '-act1' ))
        # final output
        model.add( Dense( 10, name = model_name + '-dense2' ) )
        model.add( Activation( 'tanh', name = model_name + '-act2' ) )
        # load weights
        model.load_weights( model_weight_file )
        print "INFO: successfully create baseline model from pretrained weights", model_weight_file
        model.name = model_name
        return model
    def _create_featex_model( self ) :
        '''Create feature extractor model
        '''
        model = Model( input = self.baseline.input, output = self.baseline.get_layer('flatten').output, name = 'featex' )
        return model
    def _create_clusterwise_model( self, model_weight_file, nb_hidden_nodes = 128 ) :
        '''Create finetuned clusterwise model
        '''
        model_name = os.path.basename( model_weight_file ).rsplit('.')[0]
        model = Sequential()
        model.add( Dense( nb_hidden_nodes, input_shape = ( 576, ), name = model_name + '-d1' ) )
        # activation
        model.add( Activation( 'tanh', name = model_name + '-act1' ))
        # final output
        model.add( Dense( 10, name = model_name + '-d2' ) )
        model.add( Activation( 'tanh', name = model_name + '-act2' ) )
        # load weights
        model.load_weights( model_weight_file )
        print "INFO: successfully create clusterwise model from pretrained weights", model_weight_file
        model.name = model_name
        return model
    def predict_normalized_sample( self, X, mode = 'baseline' ) :
        if ( mode == 'baseline' ) :
            return self._predict_normalized_sample_baseline( X ).reshape( [-1,5,2])
        elif ( mode in [ 'tweak', 'tweak_hard', 'hard', 'finetune' ] ) :
            fun = self._predict_normalized_sample_tweak_hard
        elif ( mode in [ 'tweak_soft', 'soft' ] ) :
            fun = self._predict_normalized_sample_tweak_soft
        else :
            raise NotImplementedError, "ERROR: prediction method %s is NOT implemented" % mode
        y = fun( X )
        y_mir = fun( X[:,:,:,::-1] )
        y_res = self._get_mirror_landmark( y_mir )
        return ( y.reshape( [ -1, 5, 2 ] ) + y_res ) / 2
        return
    def _predict_normalized_sample_baseline( self, X ) :
        '''Landmark prediction using baseline weights
        '''
        return self.baseline.predict( X, verbose = 0 )
    def _predict_normalized_sample_tweak_hard( self, X ) :
        '''Landmark prediction using tweaked models with hard membership
        '''
        f = self.featex.predict( X )
        cls = self.cluster.predict( f )
        return np.row_stack( [ self.finetune_list[this_cluster].predict( this_f.reshape( [-1, 576 ]) ) for this_cluster, this_f in zip( cls, f ) ] )
    def _predict_normalized_sample_tweak_soft( self, X ) :
        '''Landmark prediction using tweaked models with soft membership
        '''
        f = self.featex.predict( X )
        probs = np.expand_dims( self.cluster.predict_proba( f ), axis = 2 )
        lms = np.concatenate( [ np.expand_dims( this_model.predict( f ), axis = 1 ) for this_model in self.finetune_list ], axis = 1 )
        soft = np.sum( probs * lms, axis = 1 )
        return soft
    def _get_mirror_landmark( self, y ) :
        if ( y.ndim == 2 ) and ( y.size == 10 ) :
            y5x2 = y.reshape([5,2])
            y5x2 = y5x2[[1,0,2,4,3]]
            y5x2[:,0] *= -1
            return y5x2
        elif ( y.ndim == 3 ) and ( y.shape[1] == 5 ) and ( y.shape[2] == 2 ) :
            y = y[:,[1,0,2,4,3],:]
            y[:,:,0] *= -1
            return y
        elif ( y.ndim == 2 ) and ( y.shape[1] == 10 ) :
            y5x2 = y.reshape([-1, 5, 2])
            y5x2 = y5x2[:,[1,0,2,4,3],:]
            y5x2[:,:,0] *= -1
            return y5x2
        else :
            raise NotImplementedError

class Normalize( object ) :
    '''Data normalization class
    '''
    def __init__( self, D_mean, D_std, D_rule ) :
        # load D_mean
        if ( isinstance( D_mean, str ) ) :
            # D_mean is a file path
            # we should load this
            self.D_mean = np.load( D_mean );
        elif ( isinstance( D_mean, np.ndarray ) ) :
            self.D_mean = D_mean;
        else :
            raise NotImplemented, "ERROR: unknown data type %s of D_mean" % type( D_mean )
        # load D_std
        if ( isinstance( D_std, str ) ) :
            # D_mean is a file path
            # we should load this
            self.D_std = np.load( D_std );
        elif ( isinstance( D_std, np.ndarray ) ) :
            self.D_std = D_std;
        else :
            raise NotImplemented, "ERROR: unknown data type %s of D_std" % type( D_std )
        # load D_rule
        self.D_rule = D_rule
        return;
    def transform( self, D ) :
        return self.D_rule( D, self.D_mean, self.D_std );
    def save_stat( self, prefix ) :
        np.save( prefix + '-mean.npy', self.D_mean)
        print "Successfully dump mean_stat to", prefix + '-mean.npy'
        np.save( prefix + '-std.npy', self.D_std )
        print "Successfully dump std_stat to", prefix + '-std.npy'
        return prefix + '-mean.npy', prefix + '-std.npy'
