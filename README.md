# Facial Landmark Detection with Tweaked Convolutional Neural Networks (TCNN)
TCNN based facial landmark detector is a stand-alone python package for end-to-end 5-point facial landmark prediction. 
For more details about this implementation, please refer the article https://arxiv.org/pdf/1511.04031.pdf.

## Dependency 
**CNN model dependency**
  - Keras  1.2  (already included)
  - Theano 0.9
  - Make sure you correctly set up your Keras config with the Theano backend!!! (see example in `./configs`)

**Cluster model dependency**
  - Numpy 1.12
  
**Image I/O dependency**
  - OpenCV 2.4.9

**Face detection dependency**
  - Dlib 18.17

## Directory Structure

  - `libs` Keras 1.2, and tweaked landmark detector libraries.
  - `models` pretrained tweaked landmark detector weights, including both baseline weight, clusterwise finetuned tweaked model weights, and sklearn pca-gmm clustering weight.
  - `configs` contains sample keras config with the theano backend
  - `sample_data` sample images and sample testing AFLW and AFW data
  - `scripts` python scripts 
    - `test_on_alfw_afw.py` evaluates the baseline and tweaked model on ALFW and AFW dataset and plots AUC curves with additional visualization results.
    - `tweaked_facial_landmark_detector.py` standalone command-line binary for facial landmark detection.
   - `expts` experimental results for `test_on_alfw_afw.py` and `tweaked_facial_landmark_detector.py` demo output.
   
## Examples
### Evalute Performance on ALFW and AFW Dataset

Simply run the following command on your terminal

      python scripts/test_on_alfw_afw.py
Check results under `expts/test_on_alfw_afw`

| Performance on AFW | Performance on ALFW |
| ------------------ | ------------------- |

<img src="https://gitlab.com/rex-yue-wu/tweaked_landmark_predictor/raw/master/expts/test_on_alfw_afw/perf-afw_337_data.png" width="49%"> <img src="https://gitlab.com/rex-yue-wu/tweaked_landmark_predictor/raw/master/expts/test_on_alfw_afw/perf-alfw_2995_data.png" width="49%">

Visualized examples of using different prediction modes on AFW (cyan: groundtruth landmarks; purple: predicted landmarks; white: normalized MAE w.r.t. the inter-ocular distance)

| Baseline | Tweak-hard | Tweak-soft |
| -------- | ---------- | ---------- |
<img src="https://gitlab.com/rex-yue-wu/tweaked_landmark_predictor/raw/master/expts/test_on_alfw_afw/vis-afw_337_data-baseline.jpg" width="33%"> 
<img src="https://gitlab.com/rex-yue-wu/tweaked_landmark_predictor/raw/master/expts/test_on_alfw_afw/vis-afw_337_data-tweak_hard.jpg" width="33%"> 
<img src="https://gitlab.com/rex-yue-wu/tweaked_landmark_predictor/raw/master/expts/test_on_alfw_afw/vis-afw_337_data-tweak_soft.jpg" width="33%"> 


### Stand-alone Demo
Simply run the following command on your terminal

      python scripts/tweaked_facial_landmark_detector.py -m demo

This demo will take the four testing images under `sample_data/` as input, first apply `dlib` face detector to find face bounding boxes for each image, and apply the tweaked facial landmark detector under all modes for each face. All face bounding boxes and facial landmarks are saved under `expts/demo/detected_landmarks_${MODE}.csv` where `$MODE` is one of **baseline, tweak_soft, tweak_hard**. Visualization results of testing images are also saved under `expts/demo`. For example, below are the results of using `mode = tweak_soft`.

![](https://gitlab.com/rex-yue-wu/tweaked_landmark_predictor/raw/master/expts/demo/vis-tweak_soft-Zeng_Qinghong_0001.jpg)
![](https://gitlab.com/rex-yue-wu/tweaked_landmark_predictor/raw/master/expts/demo/vis-tweak_soft-Mickey_Gilley_0001.jpg)
![](https://gitlab.com/rex-yue-wu/tweaked_landmark_predictor/raw/master/expts/demo/vis-tweak_soft-obama-peru-1120.jpg)
![](https://gitlab.com/rex-yue-wu/tweaked_landmark_predictor/raw/master/expts/demo/vis-tweak_soft-Trump-kids.jpg)

## Usage
The best way to use this library is to predict facial landmark via our wrapped script, namely

      python scripts/tweaked_facial_landmark_detector.py -h
      
      usage: tweaked_facial_landmark_detector.py [-h] [-f INPUT_FILES] [-l INPUT_FLIST] [-o OUTPUT_DET_FILE] [-m {baseline,tweak_soft,tweak_hard,demo}] [-d VISUALIZATION_DIR] [--version]
      
      Keras Tweaked CNN for facial landmark detector
      
      Arguments:
        -h, --help            show this help message and exit
        -f INPUT_FILES        input test image file path
        -l INPUT_FLIST        input test image file list, each line is a file
        -o OUTPUT_DET_FILE    output detected landmark csv
        -m {baseline,tweak_soft,tweak_hard,demo}  detector working modes
        -d VISUALIZATION_DIR  output visualization dir for debugging (optional)
        --version             show program's version number and exit

For example, the command below will process two testing images and save detection results to `tmp.csv` and visualization results to the current working directory.

        python scripts/tweaked_facial_landmark_detector.py -f sample_data/obama-peru-1120.jpg -f sample_data/Trump-kids.jpg -o ./tmp.csv -d ./

The command below will process all four testing images under `baseline` mode without saving visualization results

        python scripts/tweaked_facial_landmark_detector.py -l sample_data/sample_images.list -o ./tmp.csv -m baseline


## Reference


    @ARTICLE{WuPAMI18, 
    author={Y. Wu and T. Hassner and K. Kim and G. Medioni and P. Natarajan}, 
    journal={IEEE Transactions on Pattern Analysis and Machine Intelligence}, 
    title={Facial Landmark Detection with Tweaked Convolutional Neural Networks}, 
    year={2018}, 
    pages={1-1}, 
    doi={10.1109/TPAMI.2017.2787130}, 
    ISSN={0162-8828}, 
    }
Paper Link: https://arxiv.org/pdf/1511.04031.pdf


## Contact:

> **Dr. Yue Wu**

> **Email**: yue_wu@isi.edu

> **Affiliation**: USC Information Sciences Institute


## License
The Software is made available for academic or non-commercial purposes only. The license is for a copy of the program for an unlimited term. Individuals requesting a license for commercial use must pay for a commercial license. 

      USC Stevens Institute for Innovation 
      University of Southern California 
      1150 S. Olive Street, Suite 2300 
      Los Angeles, CA 90115, USA 
      ATTN: Accounting 

DISCLAIMER. USC MAKES NO EXPRESS OR IMPLIED WARRANTIES, EITHER IN FACT OR BY OPERATION OF LAW, BY STATUTE OR OTHERWISE, AND USC SPECIFICALLY AND EXPRESSLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, VALIDITY OF THE SOFTWARE OR ANY OTHER INTELLECTUAL PROPERTY RIGHTS OR NON-INFRINGEMENT OF THE INTELLECTUAL PROPERTY OR OTHER RIGHTS OF ANY THIRD PARTY. SOFTWARE IS MADE AVAILABLE AS-IS. LIMITATION OF LIABILITY. TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT WILL USC BE LIABLE TO ANY USER OF THIS CODE FOR ANY INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE DAMAGES OF ANY KIND, LOST GOODWILL, LOST PROFITS, LOST BUSINESS AND/OR ANY INDIRECT ECONOMIC DAMAGES WHATSOEVER, REGARDLESS OF WHETHER SUCH DAMAGES ARISE FROM CLAIMS BASED UPON CONTRACT, NEGLIGENCE, TORT (INCLUDING STRICT LIABILITY OR OTHER LEGAL THEORY), A BREACH OF ANY WARRANTY OR TERM OF THIS AGREEMENT, AND REGARDLESS OF WHETHER USC WAS ADVISED OR HAD REASON TO KNOW OF THE POSSIBILITY OF INCURRING SUCH DAMAGES IN ADVANCE. 

For commercial license pricing and annual commercial update and support pricing, please contact: 

      Rakesh Pandit USC Stevens Institute for Innovation 
      University of Southern California 
      1150 S. Olive Street, Suite 2300
      Los Angeles, CA 90115, USA 

      Tel: +1 213-821-3552
      Fax: +1 213-821-5001 
      Email: rakeshvp@usc.edu and ccto: accounting@stevens.usc.edu

